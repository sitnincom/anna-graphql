"use strict";

const WebAppModule = require("anna").WebAppModule;
const bodyParser = require('body-parser');
const gse = require('graphql-server-express');
const makeExecutableSchema = require('graphql-tools').makeExecutableSchema;

const fs = require("fs");
const path = require("path");

module.exports = class GraphQLModule extends WebAppModule {
    constructor (app, config) {
        super(app, config);

        const gqlTypeDefsFilePath = path.resolve(this.app.config["graphql"].typeDefs);
        const gqlResolversFilePath = path.resolve(this.app.config["graphql"].resolvers);

        this.__schema = makeExecutableSchema({
            typeDefs: fs.readFileSync(gqlTypeDefsFilePath, "utf-8"),
            resolvers: require(gqlResolversFilePath),
        });

    }

    init () {
        this.router.use('/graphql', bodyParser.json(), gse.graphqlExpress({ schema: this.__schema }));
        this.router.use('/graphiql', gse.graphiqlExpress({endpointURL: '/graphql'}));
    }
};
